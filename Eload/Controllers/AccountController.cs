﻿using Eload.Lib;
using Eload.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eload.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            ViewBag.UserModel = Session["UserProfile"] != null ? ((UserDTO)this.Session["UserProfile"]) : null;
            ViewBag.errorMsg = TempData["isSuccess"] != null ? TempData["isSuccess"].ToString() : "";
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            ViewBag.errorMsg = TempData["Message"] != null ? TempData["Message"].ToString() : "";
            return View();
        }

        public ActionResult Info()
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Logout()
        {
            this.Session["UserProfile"] = null;
            return Redirect("~/Account/Login");
        }

        public ActionResult SystemLogin(string username, string password)
        {
            try
            {
                List<UserDTO> lstUser = UserService.SearchUser(username, HasPassword.MD5Hash(password), 1);
                Models.UserDTO userModel = lstUser.Count > 0 ? lstUser[0] : null;
                if (userModel != null && userModel.Id > 0)
                {
                    this.Session["UserProfile"] = userModel;
                    return Redirect("~/Home/Services");
                }else
                {
                    TempData["isSuccess"] = "Email hoặc mật khẩu không đúng";
                    return RedirectToAction("Login");
                }
            }
            catch (Exception ex)
            {
                TempData["isSuccess"] = ex.ToString();
                return RedirectToAction("Login");
            }
        }

        public ActionResult RegisterAccount(UserRequestDTO user)
        {
            if(user.PasswordVerify == null || user.FullName == null || user.Phone == null || user.Email == null || user.Password == null)
            {
                TempData["Message"] = "Nhập đầy đủ thông tin";
                return RedirectToAction("Register");
            }
            else
            {
                if (user.PasswordVerify != user.Password)
                {
                    TempData["Message"] = "Mật khẩu không khớp";
                    return RedirectToAction("Register");
                }
                else
                {
                    if (!UserService.CheckExitAccount(user.Email))
                    {
                        try
                        {

                            UserService.CreateNewAccount(user.FullName, user.Phone, user.Email, HasPassword.MD5Hash(user.Password));
                            return Redirect("~/Account/Login");
                        }
                        catch (Exception ex)
                        {
                            TempData["Message"] = ex.ToString();
                            return RedirectToAction("Register");
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Tài khoản đã tồn tại";
                        return RedirectToAction("Register");
                    }
                }
            }
        }
    }
}