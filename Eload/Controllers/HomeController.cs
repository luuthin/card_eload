﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eload.Models;
using Eload.Lib;
using System.Globalization;
using System.Xml.Linq;

namespace Eload.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Services()
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Fee()
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        public ActionResult KTPlus(List<TelDTO> data)
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            ViewBag.errorMsg = TempData["isError"] == "true" ? "Vui lòng nhập đầy đủ thông tin" : "";

            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                var tmp = new List<TelDTO>();
                if (data?.Count > 0 == false)
                {
                    tmp.Add(new TelDTO());
                }
                else
                {
                    tmp.AddRange(data);
                }

                return View(tmp);
            }
        }


        [HttpPost]
        public ActionResult Charge(List<TelDTO> data)
        {
            int user_id = ((UserDTO)this.Session["UserProfile"]).Id;

            for(int i = 0; i < data.Count; i++)
            {
                if (data[i].Amount == null || data[i].Amount == "" || data[i].Tel == "" || data[i].Tel == null)
                {
                    TempData["isError"] = "true";
                    return RedirectToAction("KTPlus", data);
                }
            }

            var xmlElements = new XElement("Data",
                   from item in data
                   select new XElement("Item",
                                      new XElement("tel", item.Tel),
                                      new XElement("type", item.Type),
                                      new XElement("amount", item.Amount),
                                      new XElement("user_id", user_id)
                                  ));

            TelServices.ImportTel(xmlElements);

            return RedirectToAction("KT");
        }

        [HttpPost]
        public ActionResult Import(List<TelDTO> data)
        {
            TempData["DataImport"] = data;
            return RedirectToAction("KTPlus");
        }

        public ActionResult KT(SearchTelDTO dataSearch)
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            //SearchTelDTO search = new SearchTelDTO();

            string nowStr = DateTime.Now.ToString("dd/MM/yyyy");

            // Set search default current date
            dataSearch.StartDate = dataSearch.StartDate == null ? nowStr : dataSearch.StartDate;
            dataSearch.EndDate = dataSearch.EndDate == null ? nowStr : dataSearch.EndDate;
            // Format date
            dataSearch.StartDate = DateTime.ParseExact(dataSearch.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            dataSearch.EndDate = DateTime.ParseExact(dataSearch.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<TelDTO> tels = new List<TelDTO>();
                int user_id = ((UserDTO)this.Session["UserProfile"]).Id;

                tels = TelServices.GetAllTelByUser(user_id, dataSearch.Status, dataSearch.Tel, dataSearch.StartDate, dataSearch.EndDate);
                // Format date
                dataSearch.StartDate = DateTime.ParseExact(dataSearch.StartDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                dataSearch.EndDate = DateTime.ParseExact(dataSearch.EndDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                for(int i = 0; i < tels.Count; i++)
                {
                    // "{0:n}" => 1,000.00
                    // "{0:n0}" => 1,000
                    tels[i].Amount = String.Format("{0:n0}", Convert.ToInt64(tels[i].Amount));
                }

                ViewBag.lstTel = tels;

                return View(dataSearch);
            }
        }

        public ActionResult History()
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Guide()
        {
            ViewBag.UserModel = ((UserDTO)this.Session["UserProfile"]);
            if (ViewBag.UserModel == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}