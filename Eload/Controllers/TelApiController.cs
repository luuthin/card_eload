﻿using Eload.Lib;
using Eload.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;

namespace Eload.Controllers
{
    public class TelApiController : ApiController
    {

        [HttpGet]
        [Route("api/tel/get_all")]
        public List<TelDTO> GetAll()
        {
            return TelServices.GetAllTel();
        }


        [HttpGet]
        [Route("api/tel/get_new")]
        public List<TelDTO> Get()
        {
            return TelServices.GetTelStatusNew();
        }

        [HttpPost]
        [Route("api/tel/update_status")]
        public string UpdateStatus(List<TelUpdateDTO> lstTel)
        {
            var xmlElements = new XElement("Data",
            from item in lstTel
            select new XElement("Item",
                               new XElement("id", item.Id),
                               new XElement("status", item.Status),
                               new XElement("message", item.Message),
                               new XElement("com_number", item.COMNumber),
                               new XElement("pc_name", item.PCName),
                               new XElement("e_tel", item.ETel)
                           ));

            string data = xmlElements.ToString();

            return TelServices.UpdateTelStatus(xmlElements);
        }
    }
}
