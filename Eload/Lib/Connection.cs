﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Eload.Lib
{
    public class Connection
    {
        public SqlConnection conn;
        public SqlConnection loadDB()
        {
            string constr = ConfigurationManager.ConnectionStrings["Eload"].ToString();
            conn = new SqlConnection(constr);
            return conn;
        }
        public void open(SqlConnection conn)
        {
            conn.Open();
        }
        public void close(SqlConnection conn)
        {
            conn.Close();
        }
    }
}