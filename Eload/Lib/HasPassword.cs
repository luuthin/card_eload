﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Eload.Lib
{
    public class HasPassword
    {
        public static string MD5Hash(string password)
        {
            string salt = "i!_Private_90";
            StringBuilder hash = new StringBuilder();
            byte[] bytes = Encoding.ASCII.GetBytes(salt);
            var hmacMD5 = new HMACMD5(bytes);
            byte[] saltedHash = hmacMD5.ComputeHash(new UTF8Encoding().GetBytes(password));

            for (int i = 0; i < saltedHash.Length; i++)
            {
                hash.Append(saltedHash[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}