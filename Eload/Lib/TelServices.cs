﻿using Eload.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Eload.Lib
{
    public class TelServices
    {
        private SqlConnection con;
        private static Connection connection = new Connection();
        public static List<TelDTO> GetTelStatusNew()
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetTelStatusNew";

            List<TelDTO> lstTel = new List<TelDTO>();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TelDTO telModel = new TelDTO();

                        telModel.Id = Convert.ToInt16(reader["id"].ToString());
                        telModel.Tel = reader["tel"].ToString().Trim();
                        telModel.Type = Convert.ToInt16(reader["type"].ToString());
                        telModel.Amount = reader["amount"] != null ? reader["amount"].ToString().Trim() : "";
                        telModel.Status = reader["status"] != null ? reader["status"].ToString() : "";
                        telModel.PCName = reader["pc_name"] != null ? reader["pc_name"].ToString() : "";
                        telModel.Message = reader["message"] != null ? reader["message"].ToString() : "";
                        telModel.COMNumber = reader["com_number"] != null ? reader["com_number"].ToString() : "";
                        telModel.ETel = reader["e_tel"] != null ? reader["e_tel"].ToString().Trim() : "";
                        telModel.UpdatedAt = reader["updated_at"] != null ? reader["updated_at"].ToString() : "";

                        lstTel.Add(telModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

            return lstTel;
        }

        public static List<TelDTO> GetAllTel()
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetAllTel";

            List<TelDTO> lstTel = new List<TelDTO>();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TelDTO telModel = new TelDTO();

                        telModel.Id = Convert.ToInt16(reader["id"].ToString());
                        telModel.Tel = reader["tel"].ToString().Trim();
                        telModel.Type = Convert.ToInt16(reader["type"].ToString());
                        telModel.Amount = reader["amount"] != null ? reader["amount"].ToString().Trim() : "";
                        telModel.Status = reader["status"] != null ? reader["status"].ToString() : "";
                        telModel.PCName = reader["pc_name"] != null ? reader["pc_name"].ToString() : "";
                        telModel.Message = reader["message"] != null ? reader["message"].ToString() : "";
                        telModel.COMNumber = reader["com_number"] != null ? reader["com_number"].ToString() : "";
                        telModel.ETel = reader["e_tel"] != null ? reader["e_tel"].ToString().Trim() : "";
                        telModel.UpdatedAt = reader["updated_at"] != null ? reader["updated_at"].ToString() : "";

                        lstTel.Add(telModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

            return lstTel;
        }

        public static List<TelDTO> GetAllTelByUser(int user_id, string status, string tel, string start_date, string end_date)
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetAllTelByUser";

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "UserID";
            param1.SqlDbType = SqlDbType.Int;
            param1.Value = user_id;
            cmd.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "Status";
            param2.SqlDbType = SqlDbType.NChar;
            param2.Value = status ?? (object)DBNull.Value;
            cmd.Parameters.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "Tel";
            param3.SqlDbType = SqlDbType.NChar;
            param3.Value = tel ?? (object)DBNull.Value;
            cmd.Parameters.Add(param3);

            SqlParameter param4 = new SqlParameter();
            param4.ParameterName = "StartDate";
            param4.SqlDbType = SqlDbType.NChar;
            param4.Value = start_date;
            cmd.Parameters.Add(param4);

            SqlParameter param5 = new SqlParameter();
            param5.ParameterName = "EndDate";
            param5.SqlDbType = SqlDbType.NChar;
            param5.Value = end_date;
            cmd.Parameters.Add(param5);

            List<TelDTO> lstTel = new List<TelDTO>();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TelDTO telModel = new TelDTO();

                        telModel.Id = Convert.ToInt16(reader["id"].ToString());
                        telModel.Tel = reader["tel"].ToString().Trim();
                        telModel.Type = Convert.ToInt16(reader["type"].ToString());
                        telModel.Amount = reader["amount"] != null ? reader["amount"].ToString().Trim() : "";
                        telModel.Status = reader["status"] != null ? reader["status"].ToString() : "";
                        telModel.PCName = reader["pc_name"] != null ? reader["pc_name"].ToString() : "";
                        telModel.Message = reader["message"] != null ? reader["message"].ToString() : "";
                        telModel.COMNumber = reader["com_number"] != null ? reader["com_number"].ToString() : "";
                        telModel.ETel = reader["e_tel"] != null ? reader["e_tel"].ToString().Trim() : "";
                        telModel.UpdatedAt = reader["updated_at"] != null ? reader["updated_at"].ToString() : "";

                        lstTel.Add(telModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

            return lstTel;
        }

        public static string UpdateTelStatus(XElement Data)
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "UpdateTelStatus";

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "Data";
            param1.SqlDbType = SqlDbType.Xml;
            param1.Value = new SqlXml(Data.CreateReader());
            cmd.Parameters.Add(param1);

            try
            {
                cmd.ExecuteNonQuery();
                return "UPDATED";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        public static string ImportTel(XElement Data)
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ImportTel";

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "Data";
            param1.SqlDbType = SqlDbType.Xml;
            param1.Value = new SqlXml(Data.CreateReader());
            cmd.Parameters.Add(param1);

            try
            {
                cmd.ExecuteNonQuery();
                return "IMPORTED";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                con.Close();
            }
        }

        public static void CheckAndUpdateTel()
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CheckAndUpdateTel";

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                con.Close();
            }
        }
    }
}