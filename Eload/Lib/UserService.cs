﻿using Eload.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Eload.Lib
{
    public class UserService
    {
        private SqlConnection con;
        private static Connection connection = new Connection();
        public static List<UserDTO> SearchUser(string username, string password, int typeSearch)
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "SearchUserData";

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "UserName";
            param1.SqlDbType = SqlDbType.NVarChar;
            if (username != null && username != "")
            {
                param1.Value = username;
            }
            else
            {
                param1.Value = DBNull.Value;
            }
            cmd.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "Password";
            param2.SqlDbType = SqlDbType.NVarChar;
            if (password != null && password != "")
            {
                param2.Value = password;
            }
            else
            {
                param2.Value = DBNull.Value;
            }
            cmd.Parameters.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "TypeSearch";
            param3.SqlDbType = SqlDbType.Int;
            param3.Value = typeSearch;
            cmd.Parameters.Add(param3);
            List<UserDTO> lstUser = new List<UserDTO>();
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UserDTO userModel = new UserDTO();
                        userModel.Id = Convert.ToInt16(reader["id"].ToString());
                        userModel.FullName = reader["full_name"].ToString();
                        //userModel.Password = reader["password"].ToString();
                        userModel.Email = reader["email"].ToString();
                        userModel.Phone = reader["Phone"].ToString();
                        lstUser.Add(userModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

            return lstUser;
        }

        public static bool CheckExitAccount(string email)
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CheckExitAccount";

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "Email";
            param1.SqlDbType = SqlDbType.NVarChar;
            param1.Value = email;
            cmd.Parameters.Add(param1);

            List<UserDTO> lstUser = new List<UserDTO>();
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return true;
            }
            finally
            {
                con.Close();
            }
        }

        public static string CreateNewAccount(string fullname, string phone, string email, string password)
        {
            SqlConnection con = connection.loadDB();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CreateNewAccount";

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "FullName";
            param1.SqlDbType = SqlDbType.NVarChar;
            param1.Value = fullname;
            cmd.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "Phone";
            param2.SqlDbType = SqlDbType.NVarChar;
            param2.Value = phone;
            cmd.Parameters.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "Email";
            param3.SqlDbType = SqlDbType.NVarChar;
            param3.Value = email;
            cmd.Parameters.Add(param3);

            SqlParameter param4 = new SqlParameter();
            param4.ParameterName = "Password";
            param4.SqlDbType = SqlDbType.NVarChar;
            param4.Value = password;
            cmd.Parameters.Add(param4);

            List<UserDTO> lstUser = new List<UserDTO>();
            try
            {
                cmd.ExecuteNonQuery();
                return "CREATED";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                con.Close();
            }
        }
    }
}