﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eload.Models
{
    public class ImportTelDTO
    {
       
        public string Tel { get; set; }
        public int Type { get; set; }
        public string Amount { get; set; }
    }
}