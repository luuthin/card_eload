﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eload.Models
{
    public class SearchTelDTO
    {
        private string tel;
        private string status;
        private string start_date;
        private string end_date;

        public string Tel { get => tel; set => tel = value; }
        public string Status { get => status; set => status = value; }
        public string StartDate { get => start_date; set => start_date = value; }
        public string EndDate { get => end_date; set => end_date = value; }
    }
}