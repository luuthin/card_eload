﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eload.Models
{
    public class TelDTO
    {
        private int id;
        private string tel;
        private int type;
        private string amount;
        private string status;
        private string message;
        private string pc_name;
        private string com_number;
        private string e_tel;
        private string updated_at;

        public int Id { get => id; set => id = value; }
        public string Tel { get => tel; set => tel = value; }
        public int Type { get => type; set => type = value; }
        public string Amount { get => amount; set => amount = value; }
        public string Status { get => status; set => status = value; }
        public string Message { get => message; set => message = value; }
        public string PCName { get => pc_name; set => pc_name = value; }
        public string COMNumber { get => com_number; set => com_number = value; }
        public string ETel { get => e_tel; set => e_tel = value; }
        public string UpdatedAt { get => updated_at; set => updated_at = value; }
    }
}