﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eload.Models
{
    public class TelUpdateDTO
    {
        private int id;
        private string status;
        private string message;
        private string pc_name;
        private string com_number;
        private string e_tel;

        public int Id { get => id; set => id = value; }
        public string Status { get => status; set => status = value; }
        public string Message { get => message; set => message = value; }
        public string PCName { get => pc_name; set => pc_name = value; }
        public string COMNumber { get => com_number; set => com_number = value; }
        public string ETel { get => e_tel; set => e_tel = value; }
    }
}