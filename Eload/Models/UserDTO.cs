﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eload.Models
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string CreatedAt { get; set; }
        public string UpdateAt { get; set; }
    }
}
