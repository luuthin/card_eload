﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eload.Models
{
    public class UserRequestDTO
    {
        [Required]
        [DataTypeAttribute(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string PasswordVerify { get; set; }
        [Required]
        public string Email { get; set; }
    }
}
