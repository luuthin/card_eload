USE [master]
GO
/****** Object:  Database [Eload]    Script Date: 5/3/2019 9:26:42 AM ******/
CREATE DATABASE [Eload]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Eload', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Eload.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Eload_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Eload_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Eload] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Eload].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Eload] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Eload] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Eload] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Eload] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Eload] SET ARITHABORT OFF 
GO
ALTER DATABASE [Eload] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Eload] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Eload] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Eload] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Eload] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Eload] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Eload] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Eload] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Eload] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Eload] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Eload] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Eload] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Eload] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Eload] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Eload] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Eload] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Eload] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Eload] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Eload] SET  MULTI_USER 
GO
ALTER DATABASE [Eload] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Eload] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Eload] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Eload] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Eload] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Eload]
GO
/****** Object:  Schema [HangFire]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  UserDefinedFunction [dbo].[UNIX_TIMESTAMP]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[UNIX_TIMESTAMP] (
@ctimestamp datetime
)
RETURNS integer
AS 
BEGIN
  /* Function body */
  declare @return integer

  SELECT @return = DATEDIFF(SECOND,{d '1970-01-01'}, @ctimestamp)

  return @return
END
GO
/****** Object:  Table [dbo].[Phone]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Phone](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tel] [nchar](13) NOT NULL,
	[type] [int] NULL,
	[amount] [nchar](18) NOT NULL,
	[status] [varchar](10) NULL,
	[message] [nvarchar](max) NULL,
	[pc_name] [nvarchar](max) NULL,
	[com_number] [nvarchar](max) NULL,
	[e_tel] [nchar](18) NULL,
	[user_id] [int] NOT NULL,
	[updated_at] [datetime] NULL,
	[time_start] [nchar](20) NULL,
 CONSTRAINT [PK_Phone] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[full_name] [nvarchar](max) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[phone] [nchar](13) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[created_at] [datetime] NULL,
	[update_at] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CX_HangFire_Counter]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE CLUSTERED INDEX [CX_HangFire_Counter] ON [HangFire].[Counter]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Job]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateId] [bigint] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Queue] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[List]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Server]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Set]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[State]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_HangFire_AggregatedCounter_ExpireAt]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_AggregatedCounter_ExpireAt] ON [HangFire].[AggregatedCounter]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[StateName]) 
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)
WHERE ([StateName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Server_LastHeartbeat]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Server_LastHeartbeat] ON [HangFire].[Server]
(
	[LastHeartbeat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Set_Score]    Script Date: 5/3/2019 9:26:43 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Score] ON [HangFire].[Set]
(
	[Key] ASC,
	[Score] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
/****** Object:  StoredProcedure [dbo].[CheckAndUpdateTel]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckAndUpdateTel]
AS
BEGIN
	set nocount on;

	Update Phone
	SET 
		status = 'new',
		time_start = NULL
	WHERE status = 'processing'
	AND (dbo.UNIX_TIMESTAMP(getdate()) - CONVERT(INT,LTRIM(RTRIM(time_start)))) > 300000 -- 300000 = 5 minutes

	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[CheckExitAccount]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckExitAccount] (
	@Email nvarchar(50)
	)
AS
BEGIN
	set nocount on;
	Select * from Users
	Where email = @Email
	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[CreateNewAccount]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateNewAccount] (
	@Email nvarchar(50),
	@FullName nvarchar(50),
	@Phone nvarchar(50),
	@Password nvarchar(50)
	)
AS
BEGIN
	set nocount on;
	Insert into Users(
		full_name,
		email,
		password,
		phone,
		created_at
	)
	Values(
		@FullName,
		@Email,
		@Password,
		@Phone,
		getdate()
	)

	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteTelByID]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteTelByID](
	@TelId int
)
AS
BEGIN
	set nocount on;

	Delete Phone
	WHERE id = @TelId
	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllTel]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllTel]
AS
BEGIN
	set nocount on;

	SELECT * 
	FROM Phone

	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllTelByUser]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllTelByUser](
	@UserID int,
	@Status nchar(10),
	@Tel nchar(18),
	@StartDate nchar(10),
	@EndDate nchar(10)
)
AS
BEGIN
	set nocount on;

	declare @TelLike Nvarchar(50);
	declare @EndDateTime Nvarchar(19);
	set @TelLike = N'%' + LTRIM(RTRIM(@Tel)) + '%'  --N'%123%'

	set @EndDateTime =  @EndDate + ' 23:59:59'

	print @EndDateTime

	SELECT * 
	FROM Phone
	Where user_id = @UserID
	  AND ((@Status IS NOT NULL AND status = @Status) OR @Status IS NULL)
	  AND ((@Tel IS NOT NULL AND tel like @TelLike) OR @Tel IS NULL)
	  AND (
		(@StartDate IS NOT NULL AND @EndDate IS NULL AND updated_at >= @StartDate)
		OR (@StartDate IS NULL AND @EndDate IS NOT NULL AND updated_at <= @EndDateTime)
		OR (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND updated_at >= @StartDate AND updated_at <= @EndDateTime)
		OR (@StartDate IS NULL AND @EndDate IS NULL)
	  )


	set nocount off;
END

GO
/****** Object:  StoredProcedure [dbo].[GetTelStatusNew]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTelStatusNew]
AS
BEGIN
	set nocount on;
	SELECT TOP 1 *
	INTO #TMP
	FROM Phone
	WHERE status = 'new';

	UPDATE Phone 
	SET 
		status = 'processing',
		time_start = dbo.UNIX_TIMESTAMP(getdate())
	Where id = (select id from #TMP)


	SELECT * FROM #TMP



	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[ImportTel]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ImportTel](
	@Data XML
	)
AS
BEGIN
	set nocount on;
	INSERT INTO Phone
	(
		tel,
		type,
		amount,
		user_id,
		status,
		updated_at
	)
	SELECT 
		x.i.value('./tel[1]', 'nchar(13)') AS tel,
		x.i.value('./type[1]', 'int') AS type,
		x.i.value('./amount[1]', 'nchar(18)') AS amount,
		x.i.value('./user_id[1]', 'int') AS user_id,
		'new',
		getdate()
	FROM 
	@Data.nodes('/Data/Item') as x(i);

	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[SearchUserData]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SearchUserData] (
	@UserName nvarchar(50),
	@Password  nvarchar(MAX),
	@TypeSearch int
	)
AS
BEGIN
	set nocount on;
	/*****
		TypeSearch = 0 : Không cần search theo Password chỉ cần search theo tên
		TypeSearch = 1 : Bắt buộc Search Có Password
	**/
	Select * from [Users]
	Where 
	  ((@UserName IS NOT NULL AND email = @UserName) OR 
	  (
		@TypeSearch = 0 AND @UserName IS NULL
	  )
	  )
	and (
		(@Password IS NOT NULL AND [Password] = @Password) OR 
		(@TypeSearch = 0 AND @Password IS NULL)
		)
		
	set nocount off;
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateTelStatus]    Script Date: 5/3/2019 9:26:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateTelStatus](
	@Data XML
	)
AS
BEGIN
	set nocount on;

	SELECT 
		x.i.value('./id[1]', 'int') AS id,
		x.i.value('./status[1]', 'varchar(10)') AS status,
		x.i.value('./message[1]', 'nvarchar(MAX)') AS message,
		x.i.value('./com_number[1]', 'nvarchar(MAX)') AS com_number,
		x.i.value('./pc_name[1]', 'nvarchar(MAX)') AS pc_name,
		x.i.value('./e_tel[1]', 'nvarchar(MAX)') AS e_tel
	INTO #TMP
	FROM 
	@Data.nodes('/Data/Item') as x(i);

	UPDATE Phone SET
		Phone.status = #TMP.status,
		Phone.message = #TMP.message,
		Phone.com_number = #TMP.com_number,
		Phone.pc_name = #TMP.pc_name,
		Phone.e_tel = #TMP.e_tel,
		Phone.updated_at = getdate()
	FROM Phone, #TMP 
	WHERE Phone.id = #TMP.id

	set nocount off;
END
GO
USE [master]
GO
ALTER DATABASE [Eload] SET  READ_WRITE 
GO
